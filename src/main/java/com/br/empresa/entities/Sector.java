package com.br.empresa.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sectors")
public class Sector implements Serializable{

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private String description;

  @JsonIgnore
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "id", cascade = CascadeType.ALL, orphanRemoval = true)
  private final List<Employee> employees = new ArrayList<>();

  @JsonCreator
  public Sector(){
  }

  @JsonCreator
  public Sector(@JsonProperty("id") Long id, @JsonProperty("name") String name, @JsonProperty("description") String description){
	this.id = id;
	this.name = name;
	this.description = description;
  }

  public Long getId(){
	return this.id;
  }

  public void setId(Long id){
	this.id = id;
  }

  public String getName(){
	return name;
  }

  public void setName(String name){
	this.name = name;
  }

  public String getDescription(){
	return description;
  }

  public void setDescription(String description){
	this.description = description;
  }

  public List<Employee> getEmployees(){
	return employees;
  }

  @Override
  public int hashCode(){
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0: id.hashCode());
	return result;
  }

  @Override
  public boolean equals(Object obj){
	if(this == obj)
	  return true;
	if(obj == null)
	  return false;
	if(getClass() != obj.getClass())
	  return false;
	Sector other = (Sector) obj;
	if(id == null){
	  if(other.id != null)
		return false;
	}else
	  return id.equals(other.id);
	return true;
  }

}
