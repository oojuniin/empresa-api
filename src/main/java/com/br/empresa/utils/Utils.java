package com.br.empresa.utils;

import org.springframework.expression.ParseException;

import java.time.LocalDate;

public class Utils {

	public static LocalDate conversorStringToLocalDate(String convertDate) throws ParseException{
		return LocalDate.parse(convertDate);
	}

}
