package com.br.empresa.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.br.empresa.entities.Office;
import com.br.empresa.repositories.OfficeRepository;

@Service
public class OfficeService {

	@Autowired
	private OfficeRepository repository;

	@Transactional(readOnly = true)
	public List<Office> findAll() {
		return repository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<Office> findById(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public Office save(Office cargo) {
		return repository.save(cargo);
	}

	@Transactional
	public Optional<Office> update(Office cargo, Long id) {
		Optional<Office> fromBd = repository.findById(id);
		if (fromBd.isPresent()) {
			Office update = fromBd.get();
			update.setName(cargo.getName());
			update.setDescription(cargo.getDescription());
			save(update);
			return repository.findById(update.getId());
		}
		return fromBd;
	}

	@Transactional
	public Optional<Office> delete(Long id) {
		Optional<Office> fromBd = findById(id);
		if (fromBd.isPresent()) {
			repository.deleteById(id);
		}
		return fromBd;
	}
}
