package com.br.empresa.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.br.empresa.entities.Sector;
import com.br.empresa.repositories.SectorRepository;

@Service
public class SectorService {

	@Autowired
	private SectorRepository repository;

	@Transactional(readOnly = true)
	public List<Sector> findAll() {
		return repository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<Sector> findById(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public Sector save(Sector setor) {
		return repository.save(setor);
	}

	@Transactional
	public Optional<Sector> update(Sector setor, Long id) {
		Optional<Sector> fromBd = repository.findById(id);
		if (fromBd.isPresent()) {
			Sector update = fromBd.get();
			update.setName(setor.getName());
			update.setDescription(setor.getDescription());
			save(update);
			return repository.findById(update.getId());
		}
		return fromBd;
	}

	@Transactional
	public Optional<Sector> delete(Long id) {
		Optional<Sector> fromBd = repository.findById(id);
		if (fromBd.isPresent()) {
			repository.deleteById(id);
		}
		return fromBd;
	}
}
