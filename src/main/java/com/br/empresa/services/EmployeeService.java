package com.br.empresa.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.br.empresa.entities.Employee;
import com.br.empresa.repositories.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository repository;

	@Transactional(readOnly = true)
	public List<Employee> findAll() {
		return repository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<Employee> findById(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public Employee save(Employee employee) {
		return repository.save(employee);
	}

	@Transactional
	public Optional<Employee> update(Employee employee, Long id) {
		Optional<Employee> fromBd = repository.findById(id);
		if (fromBd.isPresent()) {
			Employee update = fromBd.get();
			update.setName(employee.getName());
			update.setCpf(employee.getCpf());
			update.setEmail(employee.getEmail());
			update.setSex(employee.getSex());
			update.setBirthDate(employee.getBirthDate());
			update.setHiringDate(employee.getHiringDate());
			update.setSituation(employee.getSituation());
			update.setSector(employee.getSector());
			update.setOffice(employee.getOffice());
			save(update);
			return repository.findById(update.getId());
		}
		return fromBd;
	}

	@Transactional
	public Optional<Employee> delete(Long id) {
		Optional<Employee> fromBd = findById(id);
		if (fromBd.isPresent()) {
			repository.deleteById(id);
		}
		return fromBd;
	}
}
