package com.br.empresa.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.br.empresa.entities.Sector;
import com.br.empresa.services.SectorService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/sector")
public class SectorController {

	@Autowired
	private SectorService service;

	@GetMapping("/")
	public ResponseEntity<List<Sector>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(service.findAll());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Sector> findById(@PathVariable Long id) {
		Optional<Sector> sector = service.findById(id);
		if (sector.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.OK).body(sector.get());
	}

	@PostMapping
	public ResponseEntity<Sector> save(@RequestBody Sector sector) {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(sector));
	}

	@PutMapping("/{id}")
	public ResponseEntity<Sector> update(@RequestBody Sector setor, @PathVariable Long id) {
		Optional<Sector> setorAtualizado = service.update(setor, id);
		if (setorAtualizado.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.OK).body(setorAtualizado.get());
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Sector> delete(@PathVariable Long id) {
		Optional<Sector> deletado = service.delete(id);
		if (deletado.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.OK).body(deletado.get());
	}
}
