package com.br.empresa.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.empresa.entities.Office;
import com.br.empresa.services.OfficeService;

@RestController
@RequestMapping("/api/office")
public class OfficeController {

	@Autowired
	private OfficeService service;

	@GetMapping("/")
	public ResponseEntity<List<Office>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(service.findAll());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Office> findById(@PathVariable Long id) {
		Optional<Office> fromBd = service.findById(id);
		if (fromBd.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.OK).body(fromBd.get());
	}

	@PostMapping
	public ResponseEntity<Office> save(@RequestBody Office cargo) {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cargo));
	}

	@PutMapping("/{id}")
	public ResponseEntity<Office> update(@RequestBody Office cargo, @PathVariable Long id) {
		Optional<Office> fromBd = service.update(cargo, id);
		if (fromBd.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.OK).body(fromBd.get());
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Office> delete(@PathVariable Long id) {
		Optional<Office> fromBd = service.delete(id);
		if (fromBd.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.OK).body(fromBd.get());
	}
}
