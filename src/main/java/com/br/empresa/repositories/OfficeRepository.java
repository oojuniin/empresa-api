package com.br.empresa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.empresa.entities.Office;

@Repository
public interface OfficeRepository extends JpaRepository<Office, Long>{

}
